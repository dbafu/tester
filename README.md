
# Tester  
[![star](https://gitee.com/hamm/tester/badge/star.svg?theme=white)](https://gitee.com/hamm/tester/stargazers)
[![fork](https://gitee.com/hamm/tester/badge/fork.svg?theme=white)](https://gitee.com/hamm/tester/members)

![Tester](https://images.gitee.com/uploads/images/2019/1022/004829_9dd2f945_145025.jpeg "Tester")



#### 介绍
Tester，一款跨平台的HTTP请求测试工具，支持自定义Header，Cookies，参数等，支持快速生成API测试的Markdown文档，是一个轻量级的API测试工具。

```
为什么要写这么个狗东西？
大概是因为不太喜欢POSTMAN这个狗东西吧~
在造轮子的路上越走越远。。。
如果你喜欢，可以加QQ群：771928324
如果你喜欢但是功能不够用，你自己扩展咯~
如果你不喜欢，滚犊子，进来干嘛~
```

#### 源码运行
```
npm install && npm run
```

#### 下载安装包

- [项目附件下载](https://gitee.com/hamm/tester/attach_files)

#### TODO

1. API测试的历史记录导入导出
2. API测试的历史记录云端漫游

#### 参与贡献

1. Fork 本仓库
2. 新建分支 修改代码
3. 提交代码
4. 新建 Pull Request

[![Fork me on Gitee](https://gitee.com/hamm/tester/widgets/widget_3.svg)](https://gitee.com/hamm/tester)